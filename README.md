# Geosmc Client

A program to read GeoSciences Machine Information
   
Reads information from a database and, if necessary, updates the DNS and DHCP servers appropriately. It also reads the automount map information from  LDAP, and outputs a list of CNAME definitions suitable for inclusion in a zone file. The program stores its state and will exit quickly if nothing has changed since it last ran. The program also stores a checksum of the previously written output and only writes/creates an output file if the checksum changes.

## Build debian package

```
debuild --no-sign -us -ui -uc
```
